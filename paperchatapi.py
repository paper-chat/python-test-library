from hashlib import sha256
from random import choice, randint
import requests

SUGAR = 'KoNfEtKa'
SALT = 'red borch'
PEPPER = 'AAA! GORIT rot!'




class AllServersDeadError (Exception):
    '''
    Это исключение вызывается когда все сервера из списка недоступны
    '''


# Оно должно шифровать, по идее
class Crypter ():
    def __init__ (self, key: str or bytes):
        self.key = key
    
    def encrypt (self, data: str or bytes):
        return data
    
    def decrypt (self, data: str or bytes):
        return data



class Client ():
    def __init__ (self, client_type: str, email: str, user_password: str):
        self.client_type = client_type
        self.email = email
        self.user_password = user_password

        self.password_for_auth = sha256((self.user_password + SUGAR).encode('utf8')).hexdigest()
        self.master_key = sha256((self.user_password + SALT).encode('utf8')).hexdigest()
        self.guilds_keys_crypter = Crypter(self.master_key)

        self.guilds_cache = {} # Храним все полученный объекты гильдий ради кеширования и дедубликации
    
    def password_for_server (self, target_server_id: str):
        return sha256 ((self.password_for_auth + PEPPER + target_server_id).encode ('utf8')).hexdigest ()

class Server ():
    def __init__ (self, client: Client, server_id: str):
        self.server_id = server_id
        self.client = client
        self.key = "" # Потом сделаем
        
        protoindex = server_id.find ('/')
        portindex = server_id.find ('!')
        
        proto = server_id [:protoindex]
        address = server_id [protoindex + 1:portindex]
        port = server_id [portindex + 1:]
        
        reply = requests.get (f'{proto}://{address}:{port}/.well-known/paper-chat/server.json')
        
        self.urls = reply.json () ['servers']
    
    # Отправить запрос на случайный живой url этого сервера при помощи функции request_func
    def _request (self, request_func: callable, path: str, *args, **kwargs):
        urls = self.urls.copy ()
        
        while urls:
            q = randint (0, len (urls) - 1)
            
            reply = request_func (urls.pop (q) + path, *args, **kwargs)
            
            if reply.status_code == 200:
                return reply
        
        raise AllServersDeadError (f'Last status code is {reply.status_code}')
    
    def get (self, path: str, *args, **kwargs):
        return Server._request (self, requests.get, path, *args, **kwargs)
    
    def post (self, path: str, *args, **kwargs):
        return Server._request (self, requests.post, path, *args, **kwargs)
    
    def delete (self, path: str, *args, **kwargs):
        return Server._request (self, requests.delete, path, *args, **kwargs)
    
    def put (self, path: str, *args, **kwargs):
        return Server._request (self, requests.put, path, *args, **kwargs)
    
    def create_guild (self, name: str, description = ''):
        reply = self.post ('/guilds',
            json = {'name': name, 'description': description},
            auth = (f'{self.client.account_id}@{self.server_id}', self.client.password_for_server (self.server_id))
        )
        
        guild_id = reply.json () ['id']
        
        guild = Guild(self, guild_id)
        self.client.guilds_cache[self.server_id][guild_id] = guild

# Гильдия. Создаётся из айди, самостоятельно получает информацию о себе
class Guild ():
    def __init__ (self, server: Server, guild_id: int):
        self.server = server
        self.guild_id = guild_id
        self.name = "получаем имя запросом"
        self.description = "получаем description запросом"

# Канал либо создаётся из айди, либо из айди и имени. При получении по айди - из айди, имя он узнаёт сам.
class Channel ():
    def __init__ (self, guild: Guild, id: int):
        self.id = id
        seld.name = "получение имени по айди не реализовано"
        self.guild = guild
    def __init__ (self, guild: Guild, id: int, name: str):
        self.id = id
        seld.name = name
        self.guild = guild

# Message создаётся при отправке или получении. Тоесть нам заранее известно содержание и айди
class Message ():
    def __init__ (self, channel: Channel, content: str, id: int):
        self.channel = channel
        self.content = content
        self.id = id



# Получить время генерации айди из айди
def get_time_from_id (id: int):
    return 0 # Саня говорил что сделает

# Вернуть зарегистрированный Client
def register (home_server_id: str, email: str, username: str, password: str, client_type = 'Python paperchatapi'):
    client = Client (client_type)
    
    client.home_server = Server (home_server_id)
    
    reply = client.home_server.post ('/register', json = {'email': email, 'password': password, 'username': username})
    
    client.account_id = reply.json () ['id']
    
    client.password_hash = sha256 (password.encode ('utf8') + SUGAR).hexdigest ()
    client.master_key = sha256 (password.encode ('utf8') + SALT).hexdigest ()
